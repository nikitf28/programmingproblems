import turtle
import math

myPen = turtle.Turtle()
myPen.ht()
myPen.pencolor('blue')

myPen.speed(0)

x = 20
y = 20
a = 30

def triangle(x, y, a, depth):
  if depth == 0:
    return
  h = a * math.sqrt(3) / 2
  myPen.up()
  myPen.goto(x, y)
  myPen.down()
  myPen.goto(x + a, y)
  myPen.goto(x + a / 2, y + h)
  myPen.goto(x, y)
  myPen.up()
  triangle(x, y, a / 2, depth - 1)
  triangle(x + a / 2, y, a / 2, depth - 1)
  triangle(x + a / 4, y + h / 2, a / 2, depth - 1)


depth = int(input('Введи глубину: '))
triangle(0, 0, 500, depth)
turtle.done()