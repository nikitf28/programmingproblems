//
// Created by nikita on 23.12.2021.
//

#include "Node.h"

void Node::addNeighbour(Node *neighbour) {
    neighbours.insert(neighbour);
}

void Node::removeNeighbour(Node *neighbour) {
    neighbours.erase(neighbour);
}
