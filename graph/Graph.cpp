//
// Created by nikita on 11.11.2021.
//

#include <fstream>

#include "Graph.h"

void Graph::removeNode(Node *node) {
    nodes.erase(node);
    for (auto it = nodes.begin(); it != nodes.end(); it++){
        (*it)->removeNeighbour(node);
    }
}

void Graph::addEdge(Node* first, Node* second) {
    if (nodes.find(first) == nodes.end()){
        return;
    }
    if (nodes.find(second) == nodes.end()){
        return;
    }
    first->addNeighbour(second);
    second->addNeighbour(first);
}

void Graph::addNode(Node *node) {
    nodes.insert(node);
    for (auto it = node->nb_begin(); it != node->nb_end(); it++){
        addEdge(node, *it);
        addEdge(*it, node);
    }
}

void Graph::removeEdge(Node *first, Node *second) {
    first->removeNeighbour(second);
    second->removeNeighbour(first);
}

Graph::Graph(string fileName) {
    ifstream fin(fileName);
    string bla;
    fin >>bla >>bla;
    while (fin.eof()){
        int source, target;
        fin >>source >>target;
        Node node(to_string(source));

        for (auto it = nodes.begin(); it != nodes.end(); it++){
            if ((*it)->getName() == to_string(target)){
                node.addNeighbour((*it));
                (*it)->addNeighbour(&node);
            }
        }
    }
}

