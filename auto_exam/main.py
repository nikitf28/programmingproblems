import random
from matplotlib import pyplot as plt


def do_experiment():
    students = 100
    num_length = students
    numbers = [i for i in range(students)]
    numbers2 = [i for i in range(students)]
    boxes = [0 for i in range(students)]

    for i in range(100):
        index1 = random.randint(0, students - 1)
        index2 = random.randint(0, students - 1)
        boxes[numbers[index1]] = numbers2[index2]
        students -= 1
        numbers[index1], numbers[students] = numbers[students], numbers[index1]
        numbers2[index2], numbers2[students] = numbers2[students], numbers2[index2]

    success_rate = 0
    for student in range(100):
        i = 0
        num_now = student
        while True:
            num_now = boxes[num_now]
            if num_now == student:
                legthes.append(i)
                if i <= 50:
                    success_rate += 1
                break
            i += 1

    return success_rate


success = 0
total = 1000

results = []
legthes = []
which_try = [0, 0, 0]
lst_try = []
max_try = []

for i in range(total):
    print("NEW EXAM: " + str(i))
    max_result = 0
    last_result = 0
    for j in range(3):
        result = do_experiment()
        max_result = max(max_result, result)
        last_result = result
        results.append(result)
        if result == 100:
            which_try[j] += 1
            success += 1
            print("SUCCES: " + str(j + 1))
            break
    lst_try.append(result)
    max_try.append(max_result)

print("Succes RATE: " + str(success/total))

plt.hist(legthes, bins=100)
plt.title("Распределения длин путей, учеников: " + str(len(legthes)))
plt.show()

plt.hist(results, bins=100)
plt.title("Успешных учеников в попытке, экперементов: " + str(len(results)))
plt.show()

plt.hist(max_try, bins=100)
plt.title("Успешных учеников в максимальной попытке (из 3х), \nэкспериментов: " + str(len(max_try)))
plt.show()

plt.hist(lst_try, bins=100)
plt.title("Успешных учеников в последней попытке, \nэкспериментов: " + str(len(lst_try)))
plt.show()

labels = ['Первая', 'Вторая', 'Третья', 'Неудачная']
values = [which_try[0], which_try[1], which_try[2], total - sum(which_try)]
fig1, ax1 = plt.subplots()
ax1.pie(values, labels=labels, autopct='%.0f%%')
plt.title("С какой попытки сдали экзамен")
plt.show()