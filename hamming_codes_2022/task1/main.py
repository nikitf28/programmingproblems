import sys
import statistics

def get_hamming_distance(str1, str2):
    if len(str1) != len(str2):
        raise Exception
    difference = 0
    for ii in range(len(str1)):
        if str1[ii] != str2[ii]:
            difference += 1
    return difference


for k in range(0, 3):

    # input_filename = sys.argv[1]
    # output_filename = sys.argv[2]

    input_filename = 'Test/input' + str(k) + '.dat'
    output_filename = 'Test/output' + str(k) + '.dat'

    input_file = open(input_filename)

    codes = {}

    for line in input_file:
        info = line.split()
        codes[info[0]] = info[1:4]

    distances = [[], [], []]

    for sym in 'A', 'B', 'C', 'D':
        for comp_syb in 'A', 'B', 'C', 'D':
            if sym == comp_syb:
                continue
            place0 = get_hamming_distance(codes[comp_syb][0], codes[sym][0])
            place1 = get_hamming_distance(codes[comp_syb][1], codes[sym][1])
            place2 = get_hamming_distance(codes[comp_syb][2], codes[sym][2])

            distances[0].append(place0)

            distances[1].append(place1)

            distances[2].append(place2)

    print(distances)

    min_diff = 9999999999999
    max_avg = 0
    max_index = 0


    for i in range(3):
        if max(distances[i]) - min(distances[i]) < min_diff:
            if statistics.mean(distances[i]) > max_avg:
                    max_value = distances[i]
                    max_avg = statistics.mean(distances[i])
                    min_diff = max(distances[i]) - min(distances[i])
        print(i, max(distances[i]) - min(distances[i]), statistics.mean(distances[i]))

    realAnswer = open(output_filename).readline(1)

    if realAnswer == str(max_index + 1):
        print("Correct", end=" ")
    else:
        print("INCORRECT", end=" ")
    print(realAnswer, max_index + 1)
