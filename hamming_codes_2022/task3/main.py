input_filename = 'Test/input3.dat'
output_filename = 'Test/output3.dat'

input_file = open(input_filename)

b0 = int(input_file.readline())
b1 = int(input_file.readline())

ok0_chance = 0
ok1_chance = 0

N = int(input_file.readline())

for i in range(N):
    u, P0, P1 = map(float, input_file.readline().split())
    if u < b0:
        ok0_chance += P0
    if u < b1:
        ok1_chance += P1


input_file.readline()
M = int(input_file.readline())

for i in range(M):
    chance = []
    number = int(input_file.readline())
    while True:
        if number % 2 == 0:
            chance.append(ok0_chance)
        else:
            chance.append(ok1_chance)
        number //= 2
        if number == 0:
            break
    print("%.16f" % (1 - (sum(chance) / len(chance))))