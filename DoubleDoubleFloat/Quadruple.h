//
// Created by nikita on 20.01.2022.
//

#ifndef DOUBLEDOUBLEFLOAT_QUADRUPLE_H
#define DOUBLEDOUBLEFLOAT_QUADRUPLE_H

#include <string>
#include <ostream>
#include "QudraLong.h"
#include "rational.h"

using namespace std;

class QudrupleException{};

class Quadruple {
    unsigned long long senior = 0, junior = 0;
public:
    Quadruple();
    Quadruple(string number);

    friend ostream &operator<<(ostream &os, const Quadruple &quadruple);
    Quadruple operator *(const Quadruple& r) const;
    Quadruple operator /(const Quadruple& r) const;
    Quadruple pow(int d);
    int getMatis() const;
    void setMatis(unsigned int matis);
    bool isMinus() const;
    void setMinus(bool minus);

    Quadruple operator &(const QudraLong& r) const;
    Quadruple operator |(const QudraLong& r) const;

    Quadruple operator =(const Quadruple& r);
};


#endif //DOUBLEDOUBLEFLOAT_QUADRUPLE_H
