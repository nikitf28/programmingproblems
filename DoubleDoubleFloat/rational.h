#ifndef RATIONAL_H_INCLUDED
#define RATIONAL_H_INCLUDED
#include <iostream>
using namespace std;
class Rational{

    void simplify();
    unsigned long  long NOD(unsigned long  long a, unsigned long  long b);

public:

    Rational operator =(const Rational& r);
    Rational operator +=(const Rational& r);
    Rational operator +(const Rational& r) const;
    Rational operator -(const Rational& r) const;
    Rational operator -() const;
    Rational operator -=(const Rational& r);
    Rational operator *=(const Rational& r);
    Rational operator *(const Rational& r) const;
    Rational operator /=(const Rational& r);
    Rational operator /(const Rational& r) const;

    Rational& operator ++();
    Rational& operator --();

    bool operator ==(const Rational& r) const;
    bool operator !=(const Rational& r) const;
    bool operator >=(const Rational& r) const;
    bool operator <=(const Rational& r) const;
    bool operator >(const Rational& r) const;
    bool operator <(const Rational& r) const;

    operator unsigned long  long() const;
    operator double() const;

    friend istream& operator >>(istream& in, Rational& r);
    friend ostream& operator << (ostream& out, const Rational& r);
    unsigned long  long numer, denom;

    double toDouble();

    Rational(Rational const &r);
    Rational();
    Rational(unsigned long  long number);
    Rational(unsigned long  long n, unsigned long  long d);


};
#endif // RATIONAL_H_INCLUDED
