import numpy
from matplotlib import pyplot as plt
import math

f = open('parabola-1.dat')

f.readline()

x = []
y = []
y2 = []
x2 = []
x3 = []
x4 = []
x2y = []
xy = []

for line in f:
    data = list(map(float, line.split()))
    x.append(data[0])
    y.append(data[1])
    x2y.append((x[-1]**2) * y[-1])
    xy.append(x[-1] * y[-1])
    x2.append(x[-1] ** 2)
    x3.append(x[-1] ** 3)
    x4.append(x[-1] ** 4)

n = len(x)

inversed = numpy.linalg.inv([
    [sum(x4), sum(x3), sum(x2)],
    [sum(x3), sum(x2), sum(x)],
    [sum(x2), sum(x), n]
])
result = numpy.dot(inversed, [[sum(x2y)], [sum(xy)], [sum(y)]])

a, b, c = result

for xi in x:
    y2.append(a*(xi**2) + b * xi + c)

D_sqrt = math.sqrt(b**2 - 4 * a * c)
x1 = (-b + D_sqrt) / (2 * a)
x2 = (-b - D_sqrt) / (2 * a)

print(x1, x2)

plt.plot(x, y)
plt.plot(x, y2)
plt.plot([x1, x2], [0, 0])
plt.show()