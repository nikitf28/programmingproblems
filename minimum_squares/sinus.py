import numpy
from matplotlib import pyplot as plt
import math

points_x = []
points_y = []


def bin_search(array, num):
    l = 0
    r = len(array)
    while r - l > 1:
        c = (l + r) // 2
        if num > array[c]:
            l = c
        else:
            r = c
    return array[l], array[l-1]


def get_minimum(xi, gamma):
    eps = 1e-6
    max_iters = 10000

    for _ in range(max_iters):
        curr_x = xi
        xi, xi_1 = bin_search(x, xi)
        xi = curr_x - gamma * (func[xi] - func[xi_1]) / (- xi_1 + xi)
        xi, xi_1 = bin_search(x, xi)
        points_x.append(xi)
        points_y.append(func[xi])
        if abs(xi - curr_x) < eps:
            break
    return xi


f = open('sinus-1.dat')

f.readline()

x = []
y = []
func = {}

for line in f:
    data = list(map(float, line.split()))
    x.append(data[0])
    y.append(data[1])
    func[data[0]] = data[1]

x_ans = get_minimum(5.255255, 0.01)
print(x_ans, func[bin_search(x, x_ans)[0]])
print(points_x, points_y)
plt.plot(x, y)
plt.plot(points_x, points_y)
plt.show()
