//
// Created by nikita on 19.01.2022.
//

#ifndef SEARCHES_HUMANLIST_H
#define SEARCHES_HUMANLIST_H


#include "Human.h"
#include <iostream>
#include <map>
#include <algorithm>

class FindHumanException {};

class HumanList {
    vector<Human> list;
    vector<vector<Human> > container;
    map<string, HumanData> map_list;
public:
    HumanList();
    HumanList(string filename);
    bool readHumans(string filename);
    Human findByName(const string& name);
    Human findByName_binsearch(const string& name);
    Human findByName_binsearch(const string& name, int min, int max);
    Human findByName_map(const string& name);
    Human findByName_hash(const string& name);
    Human findByName_binsearch_hash(const string &name);
    Human findByName_binsearch_hash(const string &name, int min, int max);
    Human findByName_hash_real(const string &name);
};


#endif //SEARCHES_HUMANLIST_H
