//
// Created by nikita on 19.01.2022.
//

#include "Human.h"

istream &operator>>(istream &in, Human &human) {
    getline(in, human.name);
    string phone, addr;
    getline(in, phone);
    getline(in, addr);
    human.data.setAddress(addr);
    human.data.setPhone(phone);
    human.calcHash();
    return in;
}

ostream &operator<<(ostream &out, const Human &human) {
    out <<human.name <<", " <<human.data;
}

Human::Human(string aname, string addr, string ph) {
    this->name = aname;
    data = HumanData(addr, ph);
    calcHash();
}

Human::Human() {

}

const string &Human::getName() const {
    return name;
}

const HumanData &Human::getData() const {
    return data;
}

Human::Human(const string &name, const HumanData &data) : data(data), name(name) {
    calcHash();
}

void Human::calcHash() {
    int p = 66, p_pow = 1;
    hash = 0;
    for (int i = 0; i < name.length(); i++){
        hash += (name[i] - 'a' + 1)*p_pow;
        p_pow *= p;
    }
    hash = abs(hash)%100000;
}

bool Human::operator==(const Human &human) const {
    return (hash == human.hash && name == human.name);
}

void Human::setName(const string &name) {
    Human::name = name;
    calcHash();
}

int Human::getHash() const {
    return hash;
}

bool Human::operator<(const Human &rhs) const {
    return hash < rhs.hash;
}

bool Human::operator>(const Human &rhs) const {
    return rhs < *this;
}

bool Human::operator<=(const Human &rhs) const {
    return !(rhs < *this);
}

bool Human::operator>=(const Human &rhs) const {
    return !(*this < rhs);
}
