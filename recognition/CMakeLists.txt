cmake_minimum_required(VERSION 3.22)
project(recognition)

set(CMAKE_CXX_STANDARD 14)

add_executable(recognition
        fig/main.cpp)
